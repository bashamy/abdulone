package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.*;
import org.junit.Assert;
import pageObjects.LoginPage;

public class VeriyLogin {
	
	LoginPage lp;
	WebDriver driver;
	
	@Given("User Launch Chrome Browser")
	public void user_launch_chrome_browser() {
		//System.setProperty("webdriver.chrome.driver", "//Users//bashamy//eclipse-workspace//abdulOne//Drivers//chromedriver");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//Drivers//chromedriver");
		driver=new ChromeDriver();
		lp = new LoginPage(driver);
	}

	@When("User opens URL {string}")
	public void user_opens_url(String url) {
		driver.get(url);
	}

	@When("User enters Email as {string} and Password as {string}")
	public void user_enters_email_as_and_password_as(String email, String password) {
	    lp.setUserName(email);
	    lp.setPassword(password);
	}

	@When("Click on Login")
	public void click_on_login() {
	    lp.clickLogin();
	}
	
	@Then("Page Title should be {string}")
	public void page_title_should_be(String title) {
		/*
		if(driver.getPageSource().contains("Login was unsuccessful.")) {
			driver.close();
			Assert.assertTrue(false);
		}
		else {
			Assert.assertEquals(title, driver.getTitle());
		}
		*/
		Assert.assertEquals(title, driver.getTitle());
	}
	
	@Then("Page Source contains {string}")
	public void page_source_contains(String pageSrc) {
		if(driver.getPageSource().contains(pageSrc)) {
			Assert.assertTrue(true);
		}
		else {
			Assert.assertTrue(false);
		}
	}

	@When("User click on Log out link")
	public void user_click_on_log_out_link() {
	    lp.clickLogout();
	}

	@Then("close browser")
	public void close_browser() {
	    driver.close();
	}

}
