package testRunner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions
		(
				features = ".//Features/Login.feature",
				glue = "stepDefinitions",
				dryRun = false,
				monochrome = true,
				//tags = "@All",
				plugin = {
						"pretty",
						"json:target/LoginFeature/report.json",
						"junit:target/LoginFeature/report.xml"
				}
				
		)

public class TestRunLogin {

}
