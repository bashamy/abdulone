package TestCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestCaseTwo {
	
	@Test
	public void gmailOpen() {
		
		System.setProperty("webdriver.chrome.driver", "//Users//bashamy//eclipse-workspace//abdulOne//Drivers//chromedriver");
		WebDriver driver=new ChromeDriver();
		driver.get("http://www.gmail.com");
		String actualTitle = driver.getTitle();
		String expectedTitle = "Gmail";
		assertEquals(expectedTitle,actualTitle);
		driver.close();
		
	}

}
