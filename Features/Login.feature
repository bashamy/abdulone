Feature: Login

@Smoke @Regression
Scenario: Successful Login with Valid Credentials
	Given User Launch Chrome Browser
	When User opens URL "https://admin-demo.nopcommerce.com/login/"
	And User enters Email as "admin@yourstore.com" and Password as "admin"
	And Click on Login
	Then Page Title should be "Dashboard / nopCommerce administration"
	When User click on Log out link
	Then Page Title should be "Your store. Login"
	And close browser
	

@Regression
Scenario Outline: Failure Login with Invalid Credentials
	Given User Launch Chrome Browser
	When User opens URL "https://admin-demo.nopcommerce.com/login/"
	And User enters Email as "<userid>" and Password as "<password>"
	And Click on Login
	Then Page Title should be "Your store. Login"
	And Page Source contains "Login was unsuccessful. Please correct the errors and try again."
	Then close browser
	Examples:
	| userid| password |
	| abdul@gmail.com | password |
	| hamid@yahoo.com | login |